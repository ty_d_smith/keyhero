package UI;

import java.awt.Dimension;

import javax.swing.JButton;

import Game.Cont;

@SuppressWarnings("serial")
public class Key extends JButton{

	protected int direction;
	protected int keyCode;
	
	//Constructor: sets the dimensions for each key
	public Key(Cont cont){
		super();
		this.setSize( new Dimension(Cont.SIZE, Cont.SIZE) );		
	}
		
	//Returns: keycode
	public int getKeyCode(){
		return keyCode;
	}
	
	//Params: int x, int y
	//Sets the location of this key
	public void setPos( int x, int y ){
		this.setLocation( x, y );
	}
	
	//Parameter: boolean b
	//Sets the visibility of this key
	public void setVis( boolean b ){
		this.setVisible( b );
	}
}