package UI;

import java.awt.Image;
import java.awt.Toolkit;
import java.util.Random;

import javax.swing.ImageIcon;

import Game.Cont;

@SuppressWarnings("serial")
public class Moving extends Key{
	
	private int index;
	private Image Pic;
	
	//Constructor: sets up the throw keys 
	public Moving(Cont cont){
		super(cont);
		Random rand = new Random();
		this.index = rand.nextInt(26); 
		setPos(Cont.ThrowX[index], Cont.ThrowY[index]);
		setPic(index);
		Cont.getScreen().add(this);
		Cont.addMoving( this );
	}
	
	//Sets the images on the throw keys
	public void setPic(int index){
		if(index == 0){
			Pic = Toolkit.getDefaultToolkit().getImage("Pics/q.png");
			Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
			ImageIcon Icon = new ImageIcon( newPic );
			setIcon(Icon);
			keyCode = 81;
		}
		if(index == 1){
			Pic = Toolkit.getDefaultToolkit().getImage("Pics/w.png");
			Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
			ImageIcon Icon = new ImageIcon( newPic );
			setIcon(Icon);
			keyCode = 87;
		}
		if(index == 2){
			Pic = Toolkit.getDefaultToolkit().getImage("Pics/e.png");
			Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
			ImageIcon Icon = new ImageIcon( newPic );
			setIcon(Icon);
			keyCode = 69;
		}
		if(index == 3){
			Pic = Toolkit.getDefaultToolkit().getImage("Pics/r.png");
			Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
			ImageIcon Icon = new ImageIcon( newPic );
			setIcon(Icon);
			keyCode = 82;
		}
		if(index == 4){
			Pic = Toolkit.getDefaultToolkit().getImage("Pics/t.png");
			Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
			ImageIcon Icon = new ImageIcon( newPic );
			setIcon(Icon);
			keyCode = 84;
		}
		if(index == 5){
			Pic = Toolkit.getDefaultToolkit().getImage("Pics/y.png");
			Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
			ImageIcon Icon = new ImageIcon( newPic );
			setIcon(Icon);
			keyCode = 89;
		}
		if(index == 6){
			Pic = Toolkit.getDefaultToolkit().getImage("Pics/u.png");
			Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
			ImageIcon Icon = new ImageIcon( newPic );
			setIcon(Icon);
			keyCode = 85;
		}
		if(index == 7){
			Pic = Toolkit.getDefaultToolkit().getImage("Pics/i.png");
			Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
			ImageIcon Icon = new ImageIcon( newPic );
			setIcon(Icon);
			keyCode = 73;
		}
		if(index == 8){
			Pic = Toolkit.getDefaultToolkit().getImage("Pics/o.png");
			Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
			ImageIcon Icon = new ImageIcon( newPic );
			setIcon(Icon);
			keyCode = 79;
		}
		if(index == 9){
			Pic = Toolkit.getDefaultToolkit().getImage("Pics/p.png");
			Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
			ImageIcon Icon = new ImageIcon( newPic );
			setIcon(Icon);
			keyCode = 80;
		}
		if(index == 10){
			Pic = Toolkit.getDefaultToolkit().getImage("Pics/a.png");
			Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
			ImageIcon Icon = new ImageIcon( newPic );
			setIcon(Icon);
			keyCode = 65;
		}
		if(index == 11){
			Pic = Toolkit.getDefaultToolkit().getImage("Pics/s.png");
			Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
			ImageIcon Icon = new ImageIcon( newPic );
			setIcon(Icon);
			keyCode = 83;
		}
		if(index == 12){
			Pic = Toolkit.getDefaultToolkit().getImage("Pics/d.png");
			Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
			ImageIcon Icon = new ImageIcon( newPic );
			setIcon(Icon);
			keyCode = 68;
		}
		if(index == 13){
			Pic = Toolkit.getDefaultToolkit().getImage("Pics/f.png");
			Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
			ImageIcon Icon = new ImageIcon( newPic );
			setIcon(Icon);
			keyCode = 70;
		}
		if(index == 14){
			Pic = Toolkit.getDefaultToolkit().getImage("Pics/g.png");
			Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
			ImageIcon Icon = new ImageIcon( newPic );
			setIcon(Icon);
			keyCode = 71;
		}
		if(index == 15){
			Pic = Toolkit.getDefaultToolkit().getImage("Pics/h.png");
			Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
			ImageIcon Icon = new ImageIcon( newPic );
			setIcon(Icon);
			keyCode = 72;
		}
		if(index == 16){
			Pic = Toolkit.getDefaultToolkit().getImage("Pics/j.png");
			Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
			ImageIcon Icon = new ImageIcon( newPic );
			setIcon(Icon);
			keyCode = 74;
		}
		if(index == 17){
			Pic = Toolkit.getDefaultToolkit().getImage("Pics/k.png");
			Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
			ImageIcon Icon = new ImageIcon( newPic );
			setIcon(Icon);
			keyCode = 75;
		}
		if(index == 18){
			Pic = Toolkit.getDefaultToolkit().getImage("Pics/l.png");
			Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
			ImageIcon Icon = new ImageIcon( newPic );
			setIcon(Icon);
			keyCode = 76;
		}
		if(index == 19){
			Pic = Toolkit.getDefaultToolkit().getImage("Pics/z.png");
			Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
			ImageIcon Icon = new ImageIcon( newPic );
			setIcon(Icon);
			keyCode = 90;
		}
		if(index == 20){
			Pic = Toolkit.getDefaultToolkit().getImage("Pics/x.png");
			Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
			ImageIcon Icon = new ImageIcon( newPic );
			setIcon(Icon);
			keyCode = 88;
		}
		if(index == 21){
			Pic = Toolkit.getDefaultToolkit().getImage("Pics/c.png");
			Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
			ImageIcon Icon = new ImageIcon( newPic );
			setIcon(Icon);
			keyCode = 67;
		}
		if(index == 22){
			Pic = Toolkit.getDefaultToolkit().getImage("Pics/v.png");
			Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
			ImageIcon Icon = new ImageIcon( newPic );
			setIcon(Icon);
			keyCode = 86;
		}
		if(index == 23){
			Pic = Toolkit.getDefaultToolkit().getImage("Pics/b.png");
			Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
			ImageIcon Icon = new ImageIcon( newPic );
			setIcon(Icon);
			keyCode = 66;
		}
		if(index == 24){
			Pic = Toolkit.getDefaultToolkit().getImage("Pics/n.png");
			Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
			ImageIcon Icon = new ImageIcon( newPic );
			setIcon(Icon);
			keyCode = 78;
		}
		if(index == 25){
			Pic = Toolkit.getDefaultToolkit().getImage("Pics/m.png");
			Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
			ImageIcon Icon = new ImageIcon( newPic );
			setIcon(Icon);
			keyCode = 77;
		}
	}
}