package UI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

import Game.*;

@SuppressWarnings("serial")
public class Screen extends JPanel implements KeyListener{

	private JFrame frame;
	public ScorePane scorePane;
	private Image Pic;
	
	//Constructor: sets up the screen
	public Screen(Cont cont){
		super();
		this.setPreferredSize( new Dimension( Cont.SIZE*12+40, Cont.SIZE*13 ) );
		this.setBackground(Color.black);
		this.setLayout( null );
		addKeyListener(this);
		setFocusable(true);
		choosePic(cont);
		setFrameStats();
		scorePane = new ScorePane();
		scorePane.setBackground(Color.white);
		scorePane.setSize(new Dimension(Cont.SIZE*2+20,Cont.SIZE*13));
		scorePane.setLocation(Cont.SIZE*10+20, 0);
		this.add(scorePane);
		frame.pack();
	}
	
	//Sets up the catch keys for the constructor
	public void choosePic(Cont cont){
		for(int i=0; i<26; i++){
			Key k = new Key(cont);
			k.setPos(Cont.CatchX[i], Cont.CatchY[i]);
			if(i==0){
				Pic = Toolkit.getDefaultToolkit().getImage("Pics/q.png");
				Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
				ImageIcon Icon = new ImageIcon( newPic );
				k.setIcon(Icon);
			}
			if(i==1){
				Pic = Toolkit.getDefaultToolkit().getImage("Pics/w.png");
				Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
				ImageIcon Icon = new ImageIcon( newPic );
				k.setIcon(Icon);
			}
			if(i==2){
				Pic = Toolkit.getDefaultToolkit().getImage("Pics/e.png");
				Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
				ImageIcon Icon = new ImageIcon( newPic );
				k.setIcon(Icon);
			}
			if(i==3){
				Pic = Toolkit.getDefaultToolkit().getImage("Pics/r.png");
				Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
				ImageIcon Icon = new ImageIcon( newPic );
				k.setIcon(Icon);
			}
			if(i==4){
				Pic = Toolkit.getDefaultToolkit().getImage("Pics/t.png");
				Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
				ImageIcon Icon = new ImageIcon( newPic );
				k.setIcon(Icon);
			}
			if(i==5){
				Pic = Toolkit.getDefaultToolkit().getImage("Pics/y.png");
				Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
				ImageIcon Icon = new ImageIcon( newPic );
				k.setIcon(Icon);
			}
			if(i==6){
				Pic = Toolkit.getDefaultToolkit().getImage("Pics/u.png");
				Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
				ImageIcon Icon = new ImageIcon( newPic );
				k.setIcon(Icon);
			}
			if(i==7){
				Pic = Toolkit.getDefaultToolkit().getImage("Pics/i.png");
				Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
				ImageIcon Icon = new ImageIcon( newPic );
				k.setIcon(Icon);
			}
			if(i==8){
				Pic = Toolkit.getDefaultToolkit().getImage("Pics/o.png");
				Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
				ImageIcon Icon = new ImageIcon( newPic );
				k.setIcon(Icon);
			}
			if(i==9){
				Pic = Toolkit.getDefaultToolkit().getImage("Pics/p.png");
				Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
				ImageIcon Icon = new ImageIcon( newPic );
				k.setIcon(Icon);
			}
			if(i==10){
				Pic = Toolkit.getDefaultToolkit().getImage("Pics/a.png");
				Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
				ImageIcon Icon = new ImageIcon( newPic );
				k.setIcon(Icon);
			}
			if(i==11){
				Pic = Toolkit.getDefaultToolkit().getImage("Pics/s.png");
				Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
				ImageIcon Icon = new ImageIcon( newPic );
				k.setIcon(Icon);
			}
			if(i==12){
				Pic = Toolkit.getDefaultToolkit().getImage("Pics/d.png");
				Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
				ImageIcon Icon = new ImageIcon( newPic );
				k.setIcon(Icon);
			}
			if(i==13){
				Pic = Toolkit.getDefaultToolkit().getImage("Pics/f.png");
				Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
				ImageIcon Icon = new ImageIcon( newPic );
				k.setIcon(Icon);
			}
			if(i==14){
				Pic = Toolkit.getDefaultToolkit().getImage("Pics/g.png");
				Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
				ImageIcon Icon = new ImageIcon( newPic );
				k.setIcon(Icon);
			}
			if(i==15){
				Pic = Toolkit.getDefaultToolkit().getImage("Pics/h.png");
				Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
				ImageIcon Icon = new ImageIcon( newPic );
				k.setIcon(Icon);
			}
			if(i==16){
				Pic = Toolkit.getDefaultToolkit().getImage("Pics/j.png");
				Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
				ImageIcon Icon = new ImageIcon( newPic );
				k.setIcon(Icon);
			}
			if(i==17){
				Pic = Toolkit.getDefaultToolkit().getImage("Pics/k.png");
				Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
				ImageIcon Icon = new ImageIcon( newPic );
				k.setIcon(Icon);
			}
			if(i==18){
				Pic = Toolkit.getDefaultToolkit().getImage("Pics/l.png");
				Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
				ImageIcon Icon = new ImageIcon( newPic );
				k.setIcon(Icon);
			}
			if(i==19){
				Pic = Toolkit.getDefaultToolkit().getImage("Pics/z.png");
				Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
				ImageIcon Icon = new ImageIcon( newPic );
				k.setIcon(Icon);
			}
			if(i==20){
				Pic = Toolkit.getDefaultToolkit().getImage("Pics/x.png");
				Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
				ImageIcon Icon = new ImageIcon( newPic );
				k.setIcon(Icon);
			}
			if(i==21){
				Pic = Toolkit.getDefaultToolkit().getImage("Pics/c.png");
				Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
				ImageIcon Icon = new ImageIcon( newPic );
				k.setIcon(Icon);
			}
			if(i==22){
				Pic = Toolkit.getDefaultToolkit().getImage("Pics/v.png");
				Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
				ImageIcon Icon = new ImageIcon( newPic );
				k.setIcon(Icon);
			}
			if(i==23){
				Pic = Toolkit.getDefaultToolkit().getImage("Pics/b.png");
				Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
				ImageIcon Icon = new ImageIcon( newPic );
				k.setIcon(Icon);
			}
			if(i==24){
				Pic = Toolkit.getDefaultToolkit().getImage("Pics/n.png");
				Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
				ImageIcon Icon = new ImageIcon( newPic );
				k.setIcon(Icon);
			}
			if(i==25){
				Pic = Toolkit.getDefaultToolkit().getImage("Pics/m.png");
				Image newPic = Pic.getScaledInstance(Cont.SIZE,Cont.SIZE,java.awt.Image.SCALE_SMOOTH);
				ImageIcon Icon = new ImageIcon( newPic );
				k.setIcon(Icon);
			}
			this.add(k);
		}
	}
	
	//Sets the frame settings
	public void setFrameStats(){
		frame = new JFrame();
		frame.setVisible(true);
		frame.setResizable(false);
		frame.setLocation(500, 100);
		frame.add(this);
		frame.addKeyListener(this);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent w){
				System.exit(0);
			}
		});
	}
	
	//This is how the key presses get registered
	@Override
	public void keyPressed(KeyEvent e) {
		Game.checkPress(e.getKeyCode());
	}

	@Override
	public void keyReleased(KeyEvent e) {}
	@Override
	public void keyTyped(KeyEvent arg0) {}	
}