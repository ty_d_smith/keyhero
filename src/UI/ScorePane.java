package UI;

import javax.swing.JButton;

import Game.Game;

public class ScorePane extends JButton{

	//Constructor: sets up the score panel
	public ScorePane(){
		super();
		this.setOpaque(true);
		this.setText("Score: " + Game.getScore());
	}
	
	//Used by the checkZone method to update the score panel
	public void update(){
		this.setText("Score: " + Game.getScore());
	}
}
