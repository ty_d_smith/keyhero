package Game;

import java.util.ArrayList;

import Game.Cont;
import UI.Key;
import UI.Screen;

public class Cont {

	private static Screen scr;
	private static ArrayList<Key> moving;
	public static final int SIZE = 50;
	public static int score = 0;
	
	//set up the x and y positions for the Catch keys
	public static final int[] CatchX = new int[] { SIZE*0+10, SIZE*1+10, SIZE*2+10, SIZE*3+10,SIZE*4+10, SIZE*5+10, SIZE*6+10, SIZE*7+10,SIZE*8+10,SIZE*9+10, 
													SIZE*0+25,SIZE*1+25,SIZE*2+25,SIZE*3+25,SIZE*4+25,SIZE*5+25,SIZE*6+25,SIZE*7+25,SIZE*8+25,
													SIZE*0+50,SIZE*1+50,SIZE*2+50,SIZE*3+50,SIZE*4+50,SIZE*5+50,SIZE*6+50};
	public static final int[] CatchY = new int[] { SIZE,SIZE,SIZE,SIZE,SIZE,SIZE,SIZE,SIZE,SIZE,SIZE,
													SIZE*2+10,SIZE*2+10,SIZE*2+10,SIZE*2+10,SIZE*2+10,SIZE*2+10,SIZE*2+10,SIZE*2+10,SIZE*2+10,
													SIZE*3+20,SIZE*3+20,SIZE*3+20,SIZE*3+20,SIZE*3+20,SIZE*3+20,SIZE*3+20};
	
	//set up the x and y positions for the moving keys 
	public static final int[] ThrowX = new int[] { SIZE*0+10, SIZE*1+10, SIZE*2+10, SIZE*3+10,SIZE*4+10, SIZE*5+10, SIZE*6+10, SIZE*7+10,SIZE*8+10,SIZE*9+10, 
														SIZE*0+25,SIZE*1+25,SIZE*2+25,SIZE*3+25,SIZE*4+25,SIZE*5+25,SIZE*6+25,SIZE*7+25,SIZE*8+25,
														SIZE*0+50,SIZE*1+50,SIZE*2+50,SIZE*3+50,SIZE*4+50,SIZE*5+50,SIZE*6+50};
	public static final int[] ThrowY = new int[] { SIZE*13, SIZE*13, SIZE*13, SIZE*13,SIZE*13, SIZE*13, SIZE*13, SIZE*13,SIZE*13, SIZE*13, SIZE*13, SIZE*13,SIZE*13, SIZE*13, SIZE*13, SIZE*13,
													SIZE*13, SIZE*13, SIZE*13, SIZE*13,SIZE*13, SIZE*13, SIZE*13, SIZE*13,SIZE*13, SIZE*13};
	
	//Array of keycodes that is used by the checkPress
	public static final int[] keyCodes = new int[] {81,87,69,82,84,89,85,73,79,80,65,83,68,70,71,72,74,75,76,90,88,67,86,66,78,77}; 
	
	//Returns: the current screen
	public static Screen getScreen(){
		return Cont.scr;
	}
	
	//Returns: the keys that are seen on the current screen
	public static ArrayList<Key> getOnScreen(){
		ArrayList<Key> OnScreen = new ArrayList<Key>();
		for(Key k : Cont.moving){
			if(k.getY() > 0 && k.getY() < Cont.SIZE*9)
				OnScreen.add(k);
		}
		return OnScreen;
	}
	
	//Parameter: int k
	//Returns: index of k within the keyCodes array
	public static int getKeyCodeIndex(int k){
		for(int i = 0;i<keyCodes.length;i++){
			if(k==keyCodes[i])
				return i;
		}
		return -1;
	}
	
	//Parameter: Screen scr
	//Sets the current screen to scr
	public static void setScreen( Screen scr){
		Cont.scr = scr;
	}
	
	//Parameter: Key k
	//adds the passed in key to the ArrayList of moving keys
	public static void addMoving(Key k){
		moving.add(k);
	}
	
	//Returns: ArrayList of moving keys
	public static ArrayList<Key> getMoving(){
		return Cont.moving;
	}
	
	//Constructor: sets up the moving ArrayList
	public Cont(){
		moving = new ArrayList<Key>();
	}
}