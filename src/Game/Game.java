package Game;

import java.awt.Color;
import java.util.Random;

import Game.Cont;
import UI.Key;
import UI.Moving;
import UI.Screen;

public class Game{
	
	private static Cont cont;
	private static Screen scr;
	
	//all 4 of these are used for timing purposes
	private static long startAdd;
	private static long currentAdd;
	private static long startPaint;
	private static long currentPaint;
	
	//Overall score of the game
	private static int score;
	
	//Main: starts the game
	public static void main (String [] args){
		cont = new Cont();
		scr = new Screen(cont);
		Cont.setScreen(scr);
		play();
	}
	
	//Returns: score of the game
	public static int getScore(){
		return score;
	}
	
	//Main game method
	//Continuously runs and sets the background color
	//		-along with setting the timing for the keys to rise
	public static void play(){
		new Moving(cont);
		startPaint = System.currentTimeMillis();
		startAdd = System.currentTimeMillis();
		score = 0;
		Random rand = new Random();
		while(true){
			int R = rand.nextInt(255);
			int G = rand.nextInt(255);
			int B = rand.nextInt(255);
			currentPaint = System.currentTimeMillis();
			currentAdd = System.currentTimeMillis();
			if( currentPaint-startPaint > 10 ){
				startPaint = System.currentTimeMillis();
				for( Key k : Cont.getMoving() ){
					k.setPos(k.getX(), k.getY()-(Cont.SIZE/16));
				}
				scr.repaint();
			}

			if(currentAdd-startAdd>700){
				startAdd = System.currentTimeMillis();
				new Moving(cont);
				scr.setBackground(new Color(R,G,B));
			}
		}
	}
	
	//Calls the checkZone method
	//Sent here from the keyPress register
	public static void checkPress( int keycode ){
		checkZone(keycode);
	}
	
	//Parameter: int keyCode
	//Sets the uppermost key to the target then checks:
	//	a - if the keyCodes match
	//  b - if the zones match when the key is pressed
	//Also updates the score
	public static void checkZone( int keyCode ){
		Key target = null;
		int height = Cont.SIZE*13;
		for(Key k : Cont.getOnScreen() ){
			if( k.getY() < height && k.getY() >= 25 ){
				target = k;
				height = k.getY();
			}
		}
		
		if(Cont.CatchY[Cont.getKeyCodeIndex(keyCode)]-5 < height 
				&& height < Cont.CatchY[Cont.getKeyCodeIndex(keyCode)]+5 && target.getKeyCode()==keyCode ){
			target.setVis(false);
			score+=10;
		}else if( Cont.CatchY[Cont.getKeyCodeIndex(keyCode)]-15 < height 
				&& height < Cont.CatchY[Cont.getKeyCodeIndex(keyCode)]+15 && target.getKeyCode()==keyCode){
			target.setVis(false);
			score+=5;
		}else if(Cont.CatchY[Cont.getKeyCodeIndex(keyCode)]-25 < height 
				&& height < Cont.CatchY[Cont.getKeyCodeIndex(keyCode)]+25&& target.getKeyCode()==keyCode){
			target.setVis(false);
			score+=2;
		}
		scr.scorePane.update();
	}
}